## Checar Swapfile no Linux 

lsblk

cat /proc/swaps

cat /etc/fstab

blkid

## Desligar Swap (Entre como root)

swapoff /swapfile

rm /swapfile

## Alocar espaço no arquivo

fallocate -l 4G /swapfile

du -sh /swapfile

chmod 600 /swapfile


## Alternativa ao comando allocate
dd if=/dev/zero of=/swapfile

## Formatar arquivo para swap
mkswap /swapfile

## Ativar swap
swapon /swapfile

swapon --show

### Colocar no arquivo fstab Para ativar a montagem no boot
## fstab - Pontos de montagem automatico

vim /etc/fstab

/swapfile				none				swap    		sw    	0       0